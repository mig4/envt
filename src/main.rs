
// TODO: move to lib? {

use anyhow::{Context, Result};
use std::fs::File;
use std::io::{self, Read};
use std::path::PathBuf;
use structopt::StructOpt;
use envt::ir::ShellEnv;

/**
environment transform / tool

Extract and transform shell environment.

The currently supported parts of the environment are aliases and variables, as
these are supported in similar fashion in different shells with only some
syntax differences.
*/
#[derive(Debug, StructOpt)]
struct Cli {
    #[structopt(subcommand)]
    cmd: Command,
}

/// Parameter to define source of the internal representation
#[derive(Debug, StructOpt)]
struct IrInputOpt {
    /// file to read IR from, uses stdin if not specified
    #[structopt(short, long, parse(from_os_str))]
    file: Option<PathBuf>,
}

impl IrInputOpt {
    /// Parse the value of the input argument and return a reader
    ///
    /// If the option was not specified, or if the file is `-` return stdin,
    /// otherwise return an open file at the specified path.
    fn load(self) -> Result<ShellEnv> {
        let stdin;  // we need to hold the reference as long as we have the lock
        let mut input: Box<dyn Read> = match self.file {
            None => {
                stdin = io::stdin();
                Box::new(stdin.lock())
            },
            Some(ref f) if f.as_os_str() == "-" => {
                stdin = io::stdin();
                Box::new(stdin.lock())
            },
            Some(f) => Box::new(
                File::open(&f)
                    .with_context(|| format!("Could not open file {:?}", &f))?
            ),
        };
        // It's not a huge data structure and it's faster to read the contents in
        // anyway, see https://github.com/serde-rs/json/issues/160
        let mut s = String::new();
        input.read_to_string(&mut s)?;

        ShellEnv::from_json(&s)
    }
}

#[derive(Debug, StructOpt)]
enum Command {
    /**
    Extract environment from shell context and transform into internal representation
    */
    FromShell,

    /**
    Show / pretty-print the internal representation
    */
    Show {
        #[structopt(flatten)]
        ir_input: IrInputOpt,
    },

    /**
    Transform internal representation into a shell snippet
    */
    ToShell {
        /// name or path of the shell to generate a snippet for
        #[structopt(env)]
        shell: String,

        #[structopt(flatten)]
        ir_input: IrInputOpt,
    },
}

fn cmd_from_shell() {}

fn cmd_show(shenv: ShellEnv) -> Result<()> {
    println!("{}", serde_json::to_string_pretty(&shenv)?);
    Ok(())
}

fn cmd_to_shell(shell: String, shenv: ShellEnv) {
    eprintln!("shell = {:?}", shell);
    eprintln!("shenv = {:?}", shenv);
}

// }

fn main() -> Result<()> {
    let cli = Cli::from_args();
    eprintln!("cli = {:?}", cli);
    match cli.cmd {
        Command::FromShell => cmd_from_shell(),
        Command::Show { ir_input } => cmd_show(ir_input.load()?)?,
        Command::ToShell { shell, ir_input } => cmd_to_shell(shell, ir_input.load()?),
    };
    Ok(())
}
