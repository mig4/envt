use anyhow::{Context, Result};
use serde::{Deserialize, Serialize};

/**
 * Internal representation of a shell alias
 */
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct ShellAlias {
    /// Name of the alias
    name: String,

    /// The definition of the alias, i.e. the command to execute when alias is
    /// invoked.
    definition: String,
}

impl ShellAlias {
    pub fn new(name: &str, definition: &str) -> ShellAlias {
        ShellAlias {
            name: name.to_owned(),
            definition: definition.to_owned(),
        }
    }
}

/**
 * Internal representation of a shell variable
 */
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct ShellVar {
    /// Name of the variable
    name: String,

    /// Value assigned to the variable
    value: String,
}

impl ShellVar {
    pub fn new(name: &str, value: &str) -> ShellVar {
        ShellVar {
            name: name.to_owned(),
            value: value.to_owned(),
        }
    }
}

/**
 * Internal representation of the shell environment
 *
 * From this structure we can convert into formats appropriate for any
 * supported shell.
 */
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct ShellEnv {
    /// List of aliases
    ///
    /// Note that commands in aliases are by definition simple and have no
    /// means of parameter injection or variable replacements.
    aliases: Vec<ShellAlias>,

    /// List of variables
    ///
    /// It's best if values contain no further variable references as those
    /// would have to be evaluated by target shell and not all shells support
    /// the same syntax.
    vars: Vec<ShellVar>,
}

impl ShellEnv {
    /// Parse the given string containing JSON into a ShellEnv instance
    pub fn from_json(s: &str) -> Result<ShellEnv> {
        serde_json::from_str(s).context("Failed deserializing ShellEnv")
    }
}

#[test]
fn load_from_json() -> Result<()> {
    let senv = ShellEnv::from_json(concat!(
        r#"{"aliases":"#,
        r#"[{"name":"al1","definition":"cmd1"},{"name":"al2","definition":"cmd2"}],"#,
        r#""vars":"#,
        r#"[{"name":"v1","value":"val1"},{"name":"v2","value":"val2"}]"#,
        r#"}"#
    ))?;

    assert_eq!(
        ShellEnv {
            aliases: vec![
                ShellAlias::new("al1", "cmd1"),
                ShellAlias::new("al2", "cmd2"),
            ],
            vars: vec![
                ShellVar::new("v1", "val1"),
                ShellVar::new("v2", "val2"),
            ],
        },
        senv,
    );
    Ok(())
}
