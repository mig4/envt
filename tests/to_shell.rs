use anyhow::{Context, Result};
use assert_cmd::Command;
use predicates::prelude::*;
use std::path::{Path, PathBuf};

fn command() -> Result<Command> {
    Command::cargo_bin("envt").context("Could not get path to main command")
}

fn resource(p: &str) -> PathBuf {
    Path::new(env!("CARGO_MANIFEST_DIR")).join("tests/resources").join(p)
}

#[test]
#[ignore]
fn happy_path() -> Result<()> {
    let mut cmd = command()?;

    cmd.arg("to-shell").arg("bash")
        .pipe_stdin(resource("ir/vars-and-aliases.json"))?;
    cmd.assert()
        .success()
        .stdout(predicate::eq("\
            export VFOO='this-is-foo'
            export VBAR='this-is-bar'
            alias afoo='foo -a'
            alias abar='bar -a'\
        "));
    
    Ok(())
}
