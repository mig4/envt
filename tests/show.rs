use anyhow::{Context, Result};
use assert_cmd::Command;
use predicates::prelude::*;
use std::path::{Path, PathBuf};

fn command() -> Result<Command> {
    Command::cargo_bin("envt").context("Could not get path to main command")
}

fn resource(p: &str) -> PathBuf {
    Path::new(env!("CARGO_MANIFEST_DIR")).join("tests/resources").join(p)
}

/// Predicate that matches keys and values that should be in sample IR file
/// in `ir/vars-and-aliases.json`.
fn matches_sample_ir() -> Result<impl predicates::Predicate<str>> {
    Ok(predicate::str::is_match(r#""name":\s*"afoo""#)?
    .and(predicate::str::is_match(r#""definition":\s*"foo -a""#)?)
    .and(predicate::str::is_match(r#""name":\s*"abar""#)?)
    .and(predicate::str::is_match(r#""definition":\s*"bar -a""#)?)
    .and(predicate::str::is_match(r#""name":\s*"VBAR""#)?)
    .and(predicate::str::is_match(r#""value":\s*"this-is-bar""#)?)
    .and(predicate::str::is_match(r#""name":\s*"VFOO""#)?)
    .and(predicate::str::is_match(r#""value":\s*"this-is-foo""#)?))
}

#[test]
fn happy_path() -> Result<()> {
    let mut cmd = command()?;
    let fpath = resource("ir/vars-and-aliases.json");

    cmd.arg("show")
        .pipe_stdin(&fpath)?;
    cmd.assert()
        .success()
        .stdout(matches_sample_ir()?);

    Ok(())
}

#[test]
fn reads_from_file() -> Result<()> {
    let mut cmd = command()?;
    let fpath = resource("ir/vars-and-aliases.json");

    cmd.arg("show").arg("--file").arg(&fpath);
    cmd.assert()
        .success()
        .stdout(matches_sample_ir()?);

    Ok(())
}

#[test]
fn file_doesnt_exist() -> Result<()> {
    let mut cmd = command()?;
    let fpath = "file/doesnt/exist";

    cmd.arg("show").arg("--file").arg(fpath);
    cmd.assert()
        .failure()
        .stderr(
            predicate::str::contains(fpath)
            .and(predicate::str::contains("No such file or directory"))
        );

    Ok(())
}
